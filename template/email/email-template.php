<?php

$raw = (isset($raw)) ? $raw : [];
$body = [];
if (gettype($raw) === 'array') {
    foreach ($raw as $k) {
        $body[] = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">' . $k . '</p>';
    }
} else {
    $body[] = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">' . $raw . '</p>';
}

$body = implode('', $body);
$html = '<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Simple Transactional Email</title>
    <style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      #MessageViewBody a {
        color: inherit;
        text-decoration: none;
        font-size: inherit;
        font-family: inherit;
        font-weight: inherit;
        line-height: inherit;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
      .logo_mail {
      	width: 50%;
      }
      .redes {
      	padding: 6px 10px;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Nuevo contacto desde tusalud.vithas.es</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td COLSPAN=2 style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        ' . $body . '
                      </td>
                    </tr>
					<tr>
                    	<td style="width: 50%;background: gainsboro;">
                        	<p style="font-family: sans-serif; font-size: 11px; font-weight: normal;  margin: 0; color: gray; padding: .6rem; background-color: gainsboro;">
                        		<img src="https://docs.google.com/uc?export=download&id=1BCPwGAcxw1m7xDzTy7f_T0XChiH0DIQG" />
                        		<br>
                        		<a href="https://tusalud.vithas.es" target="_blank">tusalud.vithas.es</a>
                        	</p>
                      	</td>
                      	<td style="width: 50%;text-align: right;background: gainsboro;">
                        	<p style="font-family: sans-serif; font-size: 11px; font-weight: normal;  margin: 0; color: gray; padding: .5rem; background-color: gainsboro;">
                        		<a href="https://tusalud.vithas.es" target="_blank"><img class="redes" src="https://tusalud.vithas.es/wp-content/uploads/2019/10/twitter_mail_32.png"></a>
                        		<a href="https://tusalud.vithas.es" target="_blank"><img class="redes" src="https://tusalud.vithas.es/wp-content/uploads/2019/10/like_mail_32.png"></a>
                        		<br>
                        		<span><a href="https://tusalud.vithas.es" target="_blank">Aviso Legal</a></span> - 
                        		<span><a href="https://tusalud.vithas.es" target="_blank">Contacto</a></span>
                        	</p>
                      	</td>
                    </tr>
                    <tr>
                    	<td COLSPAN=2>							
							<p style="font-family: sans-serif; font-size: 11px; font-weight: normal;  margin: 0; color: gray;background-color: gainsboro;">
							<canvas style="background: #3e96c1;height: 1px;width: 100%"></canvas>
                            </p>
                      	</td>

                    </tr>
                    <tr>
                    	<td COLSPAN=2>							
							<p style="font-family: sans-serif; font-size: 11px; font-weight: normal;  margin: 0; color: gray; Margin-bottom: 15px; padding: .5rem; background-color: gainsboro;">Antes de imprimir este e-mail, piense bien si es realmente necesario. ADVERTENCIA: Este mensaje y sus archivos adjuntos pueden contener información confidencial y están dirigidos exclusivamente a su destinatario. Si Ud. no es el destinatario de este mensaje y lo ha recibido por error le agradeceríamos que nos lo comunicara y que procediera a destruirlo. Además le recordamos que no está autorizado a usar, imprimir, copiar o difundir este mensaje.</p>
                      	</td>

                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>



          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>';

echo $html;