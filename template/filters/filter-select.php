<?php
$response = (isset($response)) ? $response : [];
?>
<div id="vh_main_filter">
    <div class="cell-filter">
        <label for="district">Provincia</label>
        <select data-filter="district" name="district" id="select_district">
            <option value="init" selected>Seleccionar</option>
        </select>
    </div>
    <div class="cell-filter">
        <label for="center">Centro</label>
        <select data-filter="center" name="center" id="select_center">
            <option value="init" selected>Seleccionar</option>
        </select>
    </div>
    <div class="cell-filter">
        <label for="target">Especialidad</label>
        <select data-filter="target" name="target" id="select_target">
            <option value="init" selected>Seleccionar</option>
        </select>
    </div>
    <div class="cell-filter">
        <label for="type">Tipo</label>
        <select data-filter="type" name="type" id="select_type">
            <option value="init" selected>Seleccionar</option>
        </select>
    </div>
    <div>
        <button id="search_filter" type="button">Buscar</button>
    </div>
</div>
<div id="vh_response_parent">
    <?php include(__DIR__ . '/loading.php'); ?>
    <div id="filter_response"></div>
</div>

<script type="text/javascript">
    jQuery(function ($) {
        triggerFilter();
    })

</script>
